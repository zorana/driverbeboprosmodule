#include "droneOuts.h"



////// RotationAngles ////////
RotationAnglesROSModule::RotationAnglesROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

RotationAnglesROSModule::~RotationAnglesROSModule()
{

    return;
}

void RotationAnglesROSModule::init()
{

}

void RotationAnglesROSModule::close()
{

}

void RotationAnglesROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    RotationAnglesPubl = n.advertise<geometry_msgs::Vector3Stamped>("rotation_angles", 1, true);
    imu_data_pub       = n.advertise<sensor_msgs::Imu>("imu",1, true);


    //Subscriber
#ifdef no_slam_dunk
    RotationAnglesSubs=n.subscribe("states/ardrone3/PilotingState/AttitudeChanged", 1, &RotationAnglesROSModule::rotationAnglesCallback, this);
#endif

#ifdef use_slam_dunk
    slam_dunk_rotation_angles_subs = n.subscribe("/pose", 1, &RotationAnglesROSModule::slamDunkRotationAnglesCallback, this);
    slam_dunk_imu_subs             = n.subscribe("/imu",1, &RotationAnglesROSModule::slamdunkIMUCallback, this);
#endif

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool RotationAnglesROSModule::resetValues()
{
    return true;
}

//Start
bool RotationAnglesROSModule::startVal()
{
    return true;
}

//Stop
bool RotationAnglesROSModule::stopVal()
{
    return true;
}

//Run
bool RotationAnglesROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


#ifdef no_slam_dunk
void RotationAnglesROSModule::rotationAnglesCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    // Note, units of: msg->rotX, msg->rotY, msg->rotZ are radians.

    RotationAnglesMsgs.header=msg->header;
    // deg,   mavwork reference frame
    RotationAnglesMsgs.vector.x = (msg->roll)*180/M_PI;
    RotationAnglesMsgs.vector.y = (msg->pitch)*180/M_PI;
    RotationAnglesMsgs.vector.z = (msg->yaw)*180/M_PI;

    publishRotationAnglesValue();
    return;
}
#endif

#ifdef use_slam_dunk
void RotationAnglesROSModule::slamDunkRotationAnglesCallback(const geometry_msgs::PoseStamped &msg)
{
    if(!run())
        return;

    geometry_msgs::Vector3Stamped rotation_angles ;
    double roll, pitch, yaw;

    tf::Quaternion tf_quaternion;
    tf_quaternion.setX(msg.pose.orientation.x);
    tf_quaternion.setY(msg.pose.orientation.y);
    tf_quaternion.setZ(msg.pose.orientation.z);
    tf_quaternion.setW(msg.pose.orientation.w);

    tf::Matrix3x3(tf_quaternion).getRPY(roll, pitch, yaw);

    RotationAnglesMsgs.header=msg.header;
    // deg,   mavwork reference frame
    RotationAnglesMsgs.vector.x = roll*180/M_PI;
    RotationAnglesMsgs.vector.y = -pitch*180/M_PI;
    RotationAnglesMsgs.vector.z = -yaw*180/M_PI;

    publishRotationAnglesValue();
    return;

}
#endif

#ifdef use_slam_dunk
void RotationAnglesROSModule::slamdunkIMUCallback(const sensor_msgs::Imu &msg)
{
    sensor_msgs::Imu imu_data;
    imu_data.header = msg.header;

    imu_data.angular_velocity.x = msg.angular_velocity.x;
    imu_data.angular_velocity.y = msg.angular_velocity.y;
    imu_data.angular_velocity.z = msg.angular_velocity.z;

    imu_data.angular_velocity_covariance = msg.angular_velocity_covariance;

    imu_data.linear_acceleration.x = msg.linear_acceleration.x;
    imu_data.linear_acceleration.y = msg.linear_acceleration.y;
    imu_data.linear_acceleration.z = msg.linear_acceleration.z;

    imu_data.linear_acceleration_covariance = msg.linear_acceleration_covariance;

    imu_data_pub.publish(imu_data);

}

#endif


bool RotationAnglesROSModule::publishRotationAnglesValue()
{
    if(droneModuleOpened==false)
        return false;

    RotationAnglesPubl.publish(RotationAnglesMsgs);
    return true;
}


////// Battery ////////
BatteryROSModule::BatteryROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

BatteryROSModule::~BatteryROSModule()
{

    return;
}

void BatteryROSModule::init()
{

}

void BatteryROSModule::close()
{

}

void BatteryROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    BatteryPubl = n.advertise<droneMsgsROS::battery>("battery", 1, true);


    //Subscriber
    BatterySubs=n.subscribe("states/common/CommonState/BatteryStateChanged", 1, &BatteryROSModule::batteryCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool BatteryROSModule::resetValues()
{
    return true;
}

//Start
bool BatteryROSModule::startVal()
{
    return true;
}

//Stop
bool BatteryROSModule::stopVal()
{
    return true;
}

//Run
bool BatteryROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void BatteryROSModule::batteryCallback(const bebop_msgs::CommonCommonStateBatteryStateChanged::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read Battery from navdata
    BatteryMsgs.header=msg->header;
    BatteryMsgs.batteryPercent=msg->percent;

    publishBatteryValue();
    return;
}


bool BatteryROSModule::publishBatteryValue()
{
    if(droneModuleOpened==false)
        return false;

    BatteryPubl.publish(BatteryMsgs);
    return true;
}



////// Altitude ////////
AltitudeROSModule::AltitudeROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

AltitudeROSModule::~AltitudeROSModule()
{

    return;
}

void AltitudeROSModule::init()
{

}

void AltitudeROSModule::close()
{

}

void AltitudeROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    AltitudePubl = n.advertise<droneMsgsROS::droneAltitude>("altitude", 1, true);


    //Subscriber
    AltitudeSubs=n.subscribe("states/ardrone3/PilotingState/AltitudeChanged", 1, &AltitudeROSModule::altitudeCallback, this);
    //AltitudeSpeedSubs=n.subscribe("states/ardrone3/PilotingState/SpeedChanged",1,&AltitudeROSModule::altitudeSpeedCallback,this);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool AltitudeROSModule::resetValues()
{
    return true;
}

//Start
bool AltitudeROSModule::startVal()
{
    return true;
}

//Stop
bool AltitudeROSModule::stopVal()
{
    return true;
}

//Run
bool AltitudeROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void AltitudeROSModule::altitudeCallback(const bebop_msgs::Ardrone3PilotingStateAltitudeChanged::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read Altitude from navdata
    AltitudeMsgs.header=msg->header;
    //Altitude needs to be put in [m], mavwork reference frame!!
    AltitudeMsgs.altitude=-msg->altitude;
//    AltitudeMsgs.var_altitude=0.0;
//    // [m/s], mavwork reference frame
//    AltitudeMsgs.altitude_speed=
//    //AltitudeMsgs.altitude_speed=0.0;
//    AltitudeMsgs.var_altitude_speed=0.0;

    publishAltitudeValue();
    return;
}

void AltitudeROSModule::altitudeSpeedCallback(const bebop_msgs::Ardrone3PilotingStateSpeedChanged::ConstPtr& msg)
{
    if(!run())
        return;
    AltitudeMsgs.altitude_speed=-msg->speedZ;

    publishAltitudeValue();
    return;
}

bool AltitudeROSModule::publishAltitudeValue()
{
    if(droneModuleOpened==false)
        return false;

    AltitudePubl.publish(AltitudeMsgs);
    return true;
}




////// FrontCamera ////////
FrontCameraROSModule::FrontCameraROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

FrontCameraROSModule::~FrontCameraROSModule()
{

    return;
}

void FrontCameraROSModule::init()
{

}

void FrontCameraROSModule::close()
{

}

void FrontCameraROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    image_transport::ImageTransport it(n);


    //Publisher
    FrontCameraPubl = it.advertiseCamera("camera/front/image_raw", 1, true);


    //Subscriber
    FrontCameraSubs=it.subscribeCamera("image_raw", 1, &FrontCameraROSModule::frontCameraCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool FrontCameraROSModule::resetValues()
{
    return true;
}

//Start
bool FrontCameraROSModule::startVal()
{
    return true;
}

//Stop
bool FrontCameraROSModule::stopVal()
{
    return true;
}

//Run
bool FrontCameraROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void FrontCameraROSModule::frontCameraCallback(const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr & info_msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read FrontCamera
    FrontCameraMsgs=image_msg;
    FrontCameraInfoMsgs=info_msg;

    publishFrontCameraValue();
    return;
}


bool FrontCameraROSModule::publishFrontCameraValue()
{
    if(droneModuleOpened==false)
        return false;

    FrontCameraPubl.publish(FrontCameraMsgs,FrontCameraInfoMsgs);
    return true;
}





////// BottomCamera ////////
BottomCameraROSModule::BottomCameraROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

BottomCameraROSModule::~BottomCameraROSModule()
{

    return;
}

void BottomCameraROSModule::init()
{

}

void BottomCameraROSModule::close()
{

}

void BottomCameraROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    image_transport::ImageTransport it(n);


    //Publisher
    BottomCameraPubl = it.advertiseCamera("camera/bottom/image_raw", 1, true);


    //Subscriber
    BottomCameraSubs=it.subscribeCamera("image_raw", 1, &BottomCameraROSModule::bottomCameraCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool BottomCameraROSModule::resetValues()
{
    return true;
}

//Start
bool BottomCameraROSModule::startVal()
{
    return true;
}

//Stop
bool BottomCameraROSModule::stopVal()
{
    return true;
}

//Run
bool BottomCameraROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void BottomCameraROSModule::bottomCameraCallback(const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr & info_msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read BottomCamera
    BottomCameraMsgs=image_msg;
    BottomCameraInfoMsgs=info_msg;

    publishBottomCameraValue();
    return;
}


bool BottomCameraROSModule::publishBottomCameraValue()
{
    if(droneModuleOpened==false)
        return false;

    BottomCameraPubl.publish(BottomCameraMsgs,BottomCameraInfoMsgs);
    return true;
}




////// DroneStatus ////////
DroneStatusROSModule::DroneStatusROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

DroneStatusROSModule::~DroneStatusROSModule()
{

    return;
}

void DroneStatusROSModule::init()
{

}

void DroneStatusROSModule::close()
{

}

void DroneStatusROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    DroneStatusPubl = n.advertise<droneMsgsROS::droneStatus>("status", 1, true);


    //Subscriber
    DroneStatusSubs=n.subscribe("states/ardrone3/PilotingState/FlyingStateChanged", 1, &DroneStatusROSModule::droneStatusCallback, this);


    //Flag of module opened
    droneModuleOpened=true;
    droneStatusLanded();

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneStatusROSModule::resetValues()
{
    return true;
}

//Start
bool DroneStatusROSModule::startVal()
{
    return true;
}

//Stop
bool DroneStatusROSModule::stopVal()
{
    return true;
}

//Run
bool DroneStatusROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void DroneStatusROSModule::droneStatusCallback(const bebop_msgs::Ardrone3PilotingStateFlyingStateChanged::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read DroneStatus from navdata
    DroneStatusMsgs.header=msg->header;

    switch(msg->state)
    {
    case 0:
    default:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::LANDED;
        break;
    case 1:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::TAKING_OFF;
        break;
    case 2:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::HOVERING;
        break;
    case 3:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::FLYING;
        break;
    case 4:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::LANDING;
        break;
    case 5:
        DroneStatusMsgs.status=droneMsgsROS::droneStatus::EMERGENCY;
        break;
    }


    publishDroneStatusValue();
    return;
}


bool DroneStatusROSModule::publishDroneStatusValue()
{
    if(droneModuleOpened==false)
        return false;

    DroneStatusPubl.publish(DroneStatusMsgs);
    return true;
}


bool DroneStatusROSModule::droneStatusLanded()
{
    if(droneModuleOpened==false)
        return false;

    DroneStatusMsgs.status=droneMsgsROS::droneStatus::LANDED;
    DroneStatusPubl.publish(DroneStatusMsgs);
    return true;
}


////// GroundSpeed ////////
GroundSpeedROSModule::GroundSpeedROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

GroundSpeedROSModule::~GroundSpeedROSModule()
{

    return;
}

void GroundSpeedROSModule::init()
{

}

void GroundSpeedROSModule::close()
{

}

void GroundSpeedROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    GroundSpeedPubl = n.advertise<droneMsgsROS::vector2Stamped>("ground_speed", 1, true);


    //Subscriber
    GroundSpeedSubs=n.subscribe("states/ardrone3/PilotingState/SpeedChanged", 1, &GroundSpeedROSModule::groundSpeedCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool GroundSpeedROSModule::resetValues()
{
    return true;
}

//Start
bool GroundSpeedROSModule::startVal()
{
    return true;
}

//Stop
bool GroundSpeedROSModule::stopVal()
{
    return true;
}

//Run
bool GroundSpeedROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}



void GroundSpeedROSModule::groundSpeedCallback(const bebop_msgs::Ardrone3PilotingStateSpeedChanged::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read GroundSpeed from navdata
   GroundSpeedMsgs.header=msg->header;

    // [m/s], mavwork reference frame
    GroundSpeedMsgs.vector.x=msg->speedX;
    GroundSpeedMsgs.vector.y=-msg->speedY;

    publishGroundSpeedValue();
    return;
}


bool GroundSpeedROSModule::publishGroundSpeedValue()
{
    if(droneModuleOpened==false)
        return false;

    GroundSpeedPubl.publish(GroundSpeedMsgs);
    return true;
}







////// Wifi Channel ////////
WifiChannelROSModule::WifiChannelROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

WifiChannelROSModule::~WifiChannelROSModule()
{

    return;
}

void WifiChannelROSModule::init()
{

    return;
}

void WifiChannelROSModule::close()
{

}

void WifiChannelROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration

    //Publishers

    CommandPub=n.advertise<bebop_msgs::Ardrone3NetworkStateWifiAuthChannelListChanged>("wifi_channel",1,true);


    //Subscribers

    CommandSub=n.subscribe("states/ardrone3/NetworkState/WifiAuthChannelListChanged",1,&WifiChannelROSModule::wifiChannelCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;


    //End
    return;
}

//Reset
bool WifiChannelROSModule::resetValues()
{
    return true;
}

//Start
bool WifiChannelROSModule::startVal()
{
    return true;
}

//Stop
bool WifiChannelROSModule::stopVal()
{
    return true;
}

//Run
bool WifiChannelROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void WifiChannelROSModule::wifiChannelCallback(const bebop_msgs::Ardrone3NetworkStateWifiAuthChannelListChanged::ConstPtr &msg)
{
    if(!run())
        return;

    //header
    //TODO_JL

    CommandMsgs.channel=msg->channel;

    publishWifiChannelValue();

    return;
}


bool WifiChannelROSModule::publishWifiChannelValue()
{
    if(droneModuleOpened==false)
        return false;

    CommandPub.publish(CommandMsgs);
    return true;
}

