//Drone
#include "droneInps.h"


using namespace std;


////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{

    return;
}

void DroneCommandROSModule::init()
{
    CommandOutMsgs.angular.x=0.0;
    CommandOutMsgs.angular.y=0.0;
    CommandOutMsgs.angular.z=0.0;

    CommandOutMsgs.linear.x=0.0;
    CommandOutMsgs.linear.y=0.0;
    CommandOutMsgs.linear.z=0.0;


    return;
}

void DroneCommandROSModule::close()
{

}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    CommandOutPubl = n.advertise<geometry_msgs::Twist>("cmd_vel", 1, true);
    TakeOffPub=n.advertise<std_msgs::Empty>("takeoff",1, true);
    LandPub=n.advertise<std_msgs::Empty>("land",1, true);
    ResetPub=n.advertise<std_msgs::Empty>("reset",1, true);

    //services
    //getFlightAnimationServersrv = n.advertiseService("ardrone/getflightanimation",&DroneCommandROSModule::getFlightAnimationCallback,this);
    //setFlightAnimationClientsrv = n.serviceClient<ardrone_autonomy::FlightAnim>("ardrone/setflightanimation");

    //Subscribers
    PitchRollSubs=n.subscribe("command/pitch_roll", 1, &DroneCommandROSModule::pitchRollCallback, this);
    AltitudeSubs=n.subscribe("command/dAltitude", 1, &DroneCommandROSModule::dAltitudeCallback, this);
    YawSubs=n.subscribe("command/dYaw", 1, &DroneCommandROSModule::dYawCallback, this);

    CommandSubs=n.subscribe("command/high_level", 3, &DroneCommandROSModule::commandCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;


    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void DroneCommandROSModule::pitchRollCallback(const droneMsgsROS::dronePitchRollCmd::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //header
    //TODO_JL

    //Pitch
    double pitchCmd=msg->pitchCmd;
    if(pitchCmd>1.0)
        pitchCmd=1.0;
    else if(pitchCmd<-1.0)
        pitchCmd=-1.0;

    //Roll
    double rollCmd=-msg->rollCmd;
    if(rollCmd>1.0)
        rollCmd=1.0;
    else if(rollCmd<-1.0)
        rollCmd=-1.0;

    CommandOutMsgs.linear.x =  -pitchCmd;
    CommandOutMsgs.linear.y =  rollCmd;


    publishCommandValue();
    return;
}


void DroneCommandROSModule::dAltitudeCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //header
    //TODO_JL

    //dAltitude
    double dAltitudeCmd=msg->dAltitudeCmd;
    if(dAltitudeCmd>1.0)
        dAltitudeCmd=1.0;
    else if(dAltitudeCmd<-1.0)
        dAltitudeCmd=-1.0;

    CommandOutMsgs.linear.z=dAltitudeCmd;


    publishCommandValue();
    return;
}


void DroneCommandROSModule::dYawCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;


    //header
    //TODO_JL

    //dAltitude
    double dYawCmd=msg->dYawCmd;
    if(dYawCmd>1.0)
        dYawCmd=1.0;
    else if(dYawCmd<-1.0)
        dYawCmd=-1.0;

    CommandOutMsgs.angular.z=-dYawCmd;


    publishCommandValue();
    return;
}


void DroneCommandROSModule::commandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;


    switch(msg->command)
    {
    case droneMsgsROS::droneCommand::TAKE_OFF:
        //Clear command
        CommandOutMsgs.angular.x=0.0;
        CommandOutMsgs.angular.y=0.0;
        CommandOutMsgs.angular.z=0.0;
        CommandOutMsgs.linear.x=0.0;
        CommandOutMsgs.linear.y=0.0;
        CommandOutMsgs.linear.z=0.0;
        //Publish
        publishCommandValue();
        //Take off
        publishTakeOff();
        break;
    case droneMsgsROS::droneCommand::MOVE:
        //Clear
        CommandOutMsgs.angular.x=1.0;
        CommandOutMsgs.angular.y=1.0;
        //Publish
        publishCommandValue();
        break;
    case droneMsgsROS::droneCommand::LAND:
        //Clear command
        CommandOutMsgs.angular.x=0.0;
        CommandOutMsgs.angular.y=0.0;
        CommandOutMsgs.angular.z=0.0;
        CommandOutMsgs.linear.x=0.0;
        CommandOutMsgs.linear.y=0.0;
        CommandOutMsgs.linear.z=0.0;
        //Publish
        publishCommandValue();
        //Land
        publishLand();
        break;
    case droneMsgsROS::droneCommand::HOVER:
        //Clear command
        CommandOutMsgs.angular.x=0.0;
        CommandOutMsgs.angular.y=0.0;
        CommandOutMsgs.angular.z=0.0;
        CommandOutMsgs.linear.x=0.0;
        CommandOutMsgs.linear.y=0.0;
        CommandOutMsgs.linear.z=0.0;
        //Publish
        publishCommandValue();
        break;
    case droneMsgsROS::droneCommand::RESET:
        //Clear command
        CommandOutMsgs.angular.x=0.0;
        CommandOutMsgs.angular.y=0.0;
        CommandOutMsgs.angular.z=0.0;
        CommandOutMsgs.linear.x=0.0;
        CommandOutMsgs.linear.y=0.0;
        CommandOutMsgs.linear.z=0.0;
        //Publish
        publishCommandValue();
        //Reset
        publishReset();
        break;
    default:
        break;
    }

    return;
}


bool DroneCommandROSModule::publishCommandValue()
{
    if(droneModuleOpened==false)
        return false;

    CommandOutPubl.publish(CommandOutMsgs);
    return true;
}


//Take off
bool DroneCommandROSModule::publishTakeOff()
{
    if(droneModuleOpened==false)
        return false;

    TakeOffPub.publish(EmptyMsg);
    return true;
}

//Land
bool DroneCommandROSModule::publishLand()
{
    if(droneModuleOpened==false)
        return false;


    LandPub.publish(EmptyMsg);
    return true;
}

//Reset
bool DroneCommandROSModule::publishReset()
{
    if(droneModuleOpened==false)
        return false;

    ResetPub.publish(EmptyMsg);
    return true;
}

//bool DroneCommandROSModule::getFlightAnimationCallback(droneMsgsROS::getFlightAnim::Request &request,
//                                droneMsgsROS::getFlightAnim::Response &response)
//{

//    if(!run())
//        return false;

//  ardrone_autonomy::FlightAnim setFlightAnimation;


//  switch(request.AnimationMode.command)
//  {

//  case droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_AHEAD:
//  {

//      setFlightAnimation.request.type= 16;
//      setFlightAnimation.request.duration = 0;

//      break;

//  }

//  case droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_BEHIND:
//  {

//      setFlightAnimation.request.type= 17;
//      setFlightAnimation.request.duration = 0;

//      break;
//  }


//  case droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_LEFT:
//  {

//      setFlightAnimation.request.type= 18;
//      setFlightAnimation.request.duration = 0;


//      break;
//  }

//  case droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_RIGHT:
//  {

//      setFlightAnimation.request.type= 19;
//      setFlightAnimation.request.duration = 0;

//      break;
//  }

//}
//    if (setFlightAnimationClientsrv.call(setFlightAnimation))
//    {
//        response.ack=setFlightAnimation.response.result;
//    }
//    else
//    {
//        response.ack=false;
//    }
//    return response.ack;
//}




////// Gimble Command ////////
GimbalCommandROSModule::GimbalCommandROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

GimbalCommandROSModule::~GimbalCommandROSModule()
{

    return;
}

void GimbalCommandROSModule::init()
{
    CommandMsgs.angular.y=0.0;
    CommandMsgs.angular.z=0.0;

    return;
}

void GimbalCommandROSModule::close()
{

}

void GimbalCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    CommandsPub=n.advertise<geometry_msgs::Twist>("camera_control",1,true);


    //Subscribers
    
    CommandsSubs=n.subscribe("gimbal_control",1,&GimbalCommandROSModule::commandCallback, this);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //setting the camera tilt for the first time
    CommandMsgs.angular.y=3.0;
    publishGimbalCommandValue();

    //End
    return;
}

//Reset
bool GimbalCommandROSModule::resetValues()
{
    return true;
}

//Start
bool GimbalCommandROSModule::startVal()
{
    return true;
}

//Stop
bool GimbalCommandROSModule::stopVal()
{
    return true;
}

//Run
bool GimbalCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void GimbalCommandROSModule::commandCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
    if(!run())
        return;

    //header
    //TODO_JL

    //Tilt
    int tiltCmd=msg->angular.y;
    if (tiltCmd>100)
        tiltCmd=100;
    else if (tiltCmd<-100)
        tiltCmd=-100;

    //Pan
    int panCmd=msg->angular.z;
    if (panCmd>100)
        panCmd=100;
    else if (panCmd<-100)
        panCmd=-100;

    CommandMsgs.angular.y=tiltCmd;
    CommandMsgs.angular.z=panCmd;

    publishGimbalCommandValue();
    return;
}


bool GimbalCommandROSModule::publishGimbalCommandValue()
{
    if(droneModuleOpened==false)
        return false;

    CommandsPub.publish(CommandMsgs);
    return true;
}



